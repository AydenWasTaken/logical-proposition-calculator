﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE1PropositionHandling
{
    class HashCodeHandler
    {

        public string ConvertBinaryToHex(List<int> rawResults)
        {
            string resultsInHex = string.Empty;
            string currentBinary = string.Empty;

            for (int i = 0; i < rawResults.Count; i++)
            {
                currentBinary += rawResults[i].ToString();

                if (currentBinary.Length % 4 == 0 && i > 0) //if we read four values, make a hex
                {
                    resultsInHex += Convert.ToString(Convert.ToInt32(currentBinary, 2), 16);
                    currentBinary = string.Empty;
                }

                if (rawResults.Count < 4) //if there is only one variable the table has two rows so the above won't work
                {
                    if (currentBinary.Length % rawResults.Count == 0 && i > 0)
                    {
                        resultsInHex += Convert.ToString(Convert.ToInt32(currentBinary, 2), 16);
                        currentBinary = string.Empty;
                    }
                }
            }

            return resultsInHex;
        }
    }
}
