﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace ALE1PropositionHandling
{
    public partial class FormulaListEditor : Form
    {
        public FormulaListEditor()
        {
            InitializeComponent();
            MessageBox.Show("Put one formula on each line to store and use them in the main app after saving. Make sure there are no empty lines and all lines are valid logical propositions in ASCII format.");
            PopulateList();
        }

        private void PopulateList()
        {
            try
            {
                if (File.Exists("./ownFormulas.json") && new FileInfo("ownFormulas.json").Length > 2)
                {
                    using (StreamReader sr = new StreamReader("./ownFormulas.json"))
                    {
                        richTextBoxFormulas.Lines = (string[])JsonConvert.DeserializeObject(sr.ReadToEnd(), typeof(string[]));
                    }
                }
            }
            catch (Exception ex) //shouldnt happen but just in case
            {
                MessageBox.Show("Something went wrong: \n"+ex.Message);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter("./ownFormulas.json"))
                {
                    sw.Write(JsonConvert.SerializeObject(richTextBoxFormulas.Lines));
                }
                MessageBox.Show("Saved");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong: \n"+ex.Message);
            }
        }
    }
}
