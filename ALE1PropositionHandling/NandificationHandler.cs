﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE1PropositionHandling
{
    class NandificationHandler
    {
        public string Nandify(Formula formula)
        {
            string nandifiedFormula = NandifyNode(formula.RootNode);
            return nandifiedFormula;
        }

        private string NandifyNode(Node currentNode)
        {
            if (char.IsLetter(currentNode.Character)) //if its a letter, return the value given to it in the table
            {
                return currentNode.Character.ToString();
            }
            else
            {
                if (currentNode.Character == '~') //negation behavior: true if left is false
                {
                    return HandleNegation(currentNode);
                }
                else if (currentNode.Character == '>') //implication behavior: true if left is false or right is true, or both left is false and right is true
                {
                    return HandleImplication(currentNode);
                }
                else if (currentNode.Character == '=') //biimplication behavior: true if left and right has same value
                {
                    return HandleBiimplication(currentNode);
                }
                else if (currentNode.Character == '&') //conjuction behavior: only true if both left and right are true
                {
                    return HandleConjunction(currentNode);
                }
                else// if (currentNode.Character == '|') //disjunction behavior: true if left, right, or both are true
                {
                    return HandleDisjunction(currentNode);
                }
            }
        }

        //~(a) becomes %(a,a) (ascii) or a % a (infix)
        private string HandleNegation(Node currentNode)
        {
            if (char.IsLetter(currentNode.Left.Character)) //if its a letter, grab the value and return the negated one
            {
                return $"%({currentNode.Left.Character},{currentNode.Left.Character})";
            }
            else //if its an operator, return the negated value of the result of that operator
            {
                return $"%({NandifyNode(currentNode.Left)},{NandifyNode(currentNode.Left)})";
            }
        }

        //&(a,b) becomes %(%(a,b),%(a,b)) (ascii) or ( a % b ) % ( a % b ) (infix)
        private string HandleConjunction(Node currentNode)
        {
            string leftValue;
            string rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = currentNode.Left.Character.ToString();
            }
            else
            {
                leftValue = NandifyNode(currentNode.Left);
            }

            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = currentNode.Right.Character.ToString();
            }
            else
            {
                rightValue = NandifyNode(currentNode.Right);
            }

            //return the result
            return $"%(%({leftValue},{rightValue}),%({leftValue},{rightValue}))";
        }

        //or | becomes %(%(a,a),%(b,b)) (ascii) or ( a % a ) % ( b % b ) (infix)
        private string HandleDisjunction(Node currentNode)
        {
            string leftValue;
            string rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = currentNode.Left.Character.ToString();
            }
            else
            {
                leftValue = NandifyNode(currentNode.Left);
            }

            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = currentNode.Right.Character.ToString();
            }
            else
            {
                rightValue = NandifyNode(currentNode.Right);
            }

            //return the result
            return $"%(%({leftValue},{leftValue}),%({rightValue},{rightValue}))";
        }

        //not left or right >(a,b) becomes %(a,%(b,b)) (ascii) or a % ( b % b ) (infix)
        private string HandleImplication(Node currentNode)
        {
            string leftValue;
            string rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = currentNode.Left.Character.ToString();
            }
            else
            {
                leftValue = NandifyNode(currentNode.Left);
            }

            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = currentNode.Right.Character.ToString();
            }
            else
            {
                rightValue = NandifyNode(currentNode.Right);
            }

            //return the result
            return $"%({leftValue},%({rightValue},{rightValue}))";
        }

        //the same =(a,b) becomes %(%(%(a,a),%(b,b)),%(a,b)) (ascii) or ( ( a % a ) % ( b % b ) ) % ( a % b ) (infix)
        private string HandleBiimplication(Node currentNode)
        {
            string leftValue;
            string rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = currentNode.Left.Character.ToString();
            }
            else
            {
                leftValue = NandifyNode(currentNode.Left);
            }

            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = currentNode.Right.Character.ToString();
            }
            else
            {
                rightValue = NandifyNode(currentNode.Right);
            }

            //return the result
            return $"%(%(%({leftValue},{leftValue}),%({rightValue},{rightValue})),%({leftValue},{rightValue}))";
        }
    }
}
