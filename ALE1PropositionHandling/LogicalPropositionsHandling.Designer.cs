﻿namespace ALE1PropositionHandling
{
    partial class LogicalPropositionsHandling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInputFormula = new System.Windows.Forms.Label();
            this.rtbInputFormula = new System.Windows.Forms.RichTextBox();
            this.btnCalculateStuff = new System.Windows.Forms.Button();
            this.tabControlCalculationResults = new System.Windows.Forms.TabControl();
            this.tabPageResultsData = new System.Windows.Forms.TabPage();
            this.textboxNormalizedFormula = new System.Windows.Forms.RichTextBox();
            this.textboxNormalizedInfix = new System.Windows.Forms.RichTextBox();
            this.textboxNandification = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxHashCodeResult = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxResultInfixNotation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxVariablesInFormula = new System.Windows.Forms.TextBox();
            this.tabPageBinaryTree = new System.Windows.Forms.TabPage();
            this.pictureBoxBinaryTree = new System.Windows.Forms.PictureBox();
            this.tabPageTruthTable = new System.Windows.Forms.TabPage();
            this.listboxTruthTable = new System.Windows.Forms.ListBox();
            this.tabPageSimplifiedTruthTable = new System.Windows.Forms.TabPage();
            this.listboxSimplifiedTruthTable = new System.Windows.Forms.ListBox();
            this.tabPageNormalizedTruthTable = new System.Windows.Forms.TabPage();
            this.listboxNormalizedTruthTable = new System.Windows.Forms.ListBox();
            this.tabPageNormalizedSimplifiedTable = new System.Windows.Forms.TabPage();
            this.listboxNormalizedSimplifiedTruthTable = new System.Windows.Forms.ListBox();
            this.buttonGetRandomFormulaFromCollection = new System.Windows.Forms.Button();
            this.buttonEditList = new System.Windows.Forms.Button();
            this.buttonCalculateNextOwnFormula = new System.Windows.Forms.Button();
            this.tabControlCalculationResults.SuspendLayout();
            this.tabPageResultsData.SuspendLayout();
            this.tabPageBinaryTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBinaryTree)).BeginInit();
            this.tabPageTruthTable.SuspendLayout();
            this.tabPageSimplifiedTruthTable.SuspendLayout();
            this.tabPageNormalizedTruthTable.SuspendLayout();
            this.tabPageNormalizedSimplifiedTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInputFormula
            // 
            this.lblInputFormula.AutoSize = true;
            this.lblInputFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputFormula.Location = new System.Drawing.Point(31, 42);
            this.lblInputFormula.Name = "lblInputFormula";
            this.lblInputFormula.Size = new System.Drawing.Size(134, 32);
            this.lblInputFormula.TabIndex = 1;
            this.lblInputFormula.Text = "Formula: ";
            // 
            // rtbInputFormula
            // 
            this.rtbInputFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInputFormula.Location = new System.Drawing.Point(199, 12);
            this.rtbInputFormula.Name = "rtbInputFormula";
            this.rtbInputFormula.Size = new System.Drawing.Size(2107, 96);
            this.rtbInputFormula.TabIndex = 3;
            this.rtbInputFormula.Text = "";
            // 
            // btnCalculateStuff
            // 
            this.btnCalculateStuff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculateStuff.Location = new System.Drawing.Point(12, 114);
            this.btnCalculateStuff.Name = "btnCalculateStuff";
            this.btnCalculateStuff.Size = new System.Drawing.Size(174, 145);
            this.btnCalculateStuff.TabIndex = 5;
            this.btnCalculateStuff.Text = "Calculate all the things";
            this.btnCalculateStuff.UseVisualStyleBackColor = true;
            this.btnCalculateStuff.Click += new System.EventHandler(this.btnCalculateStuff_Click);
            // 
            // tabControlCalculationResults
            // 
            this.tabControlCalculationResults.Controls.Add(this.tabPageResultsData);
            this.tabControlCalculationResults.Controls.Add(this.tabPageBinaryTree);
            this.tabControlCalculationResults.Controls.Add(this.tabPageTruthTable);
            this.tabControlCalculationResults.Controls.Add(this.tabPageSimplifiedTruthTable);
            this.tabControlCalculationResults.Controls.Add(this.tabPageNormalizedTruthTable);
            this.tabControlCalculationResults.Controls.Add(this.tabPageNormalizedSimplifiedTable);
            this.tabControlCalculationResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlCalculationResults.Location = new System.Drawing.Point(192, 114);
            this.tabControlCalculationResults.Name = "tabControlCalculationResults";
            this.tabControlCalculationResults.SelectedIndex = 0;
            this.tabControlCalculationResults.Size = new System.Drawing.Size(2118, 791);
            this.tabControlCalculationResults.TabIndex = 0;
            // 
            // tabPageResultsData
            // 
            this.tabPageResultsData.Controls.Add(this.textboxNormalizedFormula);
            this.tabPageResultsData.Controls.Add(this.textboxNormalizedInfix);
            this.tabPageResultsData.Controls.Add(this.textboxNandification);
            this.tabPageResultsData.Controls.Add(this.label6);
            this.tabPageResultsData.Controls.Add(this.label5);
            this.tabPageResultsData.Controls.Add(this.label4);
            this.tabPageResultsData.Controls.Add(this.textBoxHashCodeResult);
            this.tabPageResultsData.Controls.Add(this.label1);
            this.tabPageResultsData.Controls.Add(this.label3);
            this.tabPageResultsData.Controls.Add(this.textBoxResultInfixNotation);
            this.tabPageResultsData.Controls.Add(this.label2);
            this.tabPageResultsData.Controls.Add(this.textBoxVariablesInFormula);
            this.tabPageResultsData.Location = new System.Drawing.Point(4, 41);
            this.tabPageResultsData.Name = "tabPageResultsData";
            this.tabPageResultsData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResultsData.Size = new System.Drawing.Size(2110, 746);
            this.tabPageResultsData.TabIndex = 1;
            this.tabPageResultsData.Text = "Data results";
            this.tabPageResultsData.UseVisualStyleBackColor = true;
            // 
            // textboxNormalizedFormula
            // 
            this.textboxNormalizedFormula.Location = new System.Drawing.Point(270, 169);
            this.textboxNormalizedFormula.Name = "textboxNormalizedFormula";
            this.textboxNormalizedFormula.ReadOnly = true;
            this.textboxNormalizedFormula.Size = new System.Drawing.Size(1805, 178);
            this.textboxNormalizedFormula.TabIndex = 12;
            this.textboxNormalizedFormula.Text = "";
            // 
            // textboxNormalizedInfix
            // 
            this.textboxNormalizedInfix.Location = new System.Drawing.Point(270, 360);
            this.textboxNormalizedInfix.Name = "textboxNormalizedInfix";
            this.textboxNormalizedInfix.ReadOnly = true;
            this.textboxNormalizedInfix.Size = new System.Drawing.Size(1805, 178);
            this.textboxNormalizedInfix.TabIndex = 11;
            this.textboxNormalizedInfix.Text = "";
            // 
            // textboxNandification
            // 
            this.textboxNandification.Location = new System.Drawing.Point(270, 544);
            this.textboxNandification.Name = "textboxNandification";
            this.textboxNandification.ReadOnly = true;
            this.textboxNandification.Size = new System.Drawing.Size(1805, 178);
            this.textboxNandification.TabIndex = 7;
            this.textboxNandification.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 547);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 32);
            this.label6.TabIndex = 10;
            this.label6.Text = "Nandified:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 363);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 32);
            this.label5.TabIndex = 8;
            this.label5.Text = "Normalized infix:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 32);
            this.label4.TabIndex = 6;
            this.label4.Text = "Normalized:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxHashCodeResult
            // 
            this.textBoxHashCodeResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHashCodeResult.Location = new System.Drawing.Point(270, 114);
            this.textBoxHashCodeResult.Name = "textBoxHashCodeResult";
            this.textBoxHashCodeResult.ReadOnly = true;
            this.textBoxHashCodeResult.Size = new System.Drawing.Size(1805, 39);
            this.textBoxHashCodeResult.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 32);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hash code of result:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 32);
            this.label3.TabIndex = 3;
            this.label3.Text = "Infix: ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultInfixNotation
            // 
            this.textBoxResultInfixNotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxResultInfixNotation.Location = new System.Drawing.Point(270, 60);
            this.textBoxResultInfixNotation.Name = "textBoxResultInfixNotation";
            this.textBoxResultInfixNotation.ReadOnly = true;
            this.textBoxResultInfixNotation.Size = new System.Drawing.Size(1805, 39);
            this.textBoxResultInfixNotation.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Variables: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxVariablesInFormula
            // 
            this.textBoxVariablesInFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxVariablesInFormula.Location = new System.Drawing.Point(270, 6);
            this.textBoxVariablesInFormula.Name = "textBoxVariablesInFormula";
            this.textBoxVariablesInFormula.ReadOnly = true;
            this.textBoxVariablesInFormula.Size = new System.Drawing.Size(1805, 39);
            this.textBoxVariablesInFormula.TabIndex = 0;
            // 
            // tabPageBinaryTree
            // 
            this.tabPageBinaryTree.Controls.Add(this.pictureBoxBinaryTree);
            this.tabPageBinaryTree.Location = new System.Drawing.Point(4, 41);
            this.tabPageBinaryTree.Name = "tabPageBinaryTree";
            this.tabPageBinaryTree.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBinaryTree.Size = new System.Drawing.Size(2110, 746);
            this.tabPageBinaryTree.TabIndex = 0;
            this.tabPageBinaryTree.Text = "Binary Tree";
            this.tabPageBinaryTree.UseVisualStyleBackColor = true;
            // 
            // pictureBoxBinaryTree
            // 
            this.pictureBoxBinaryTree.Location = new System.Drawing.Point(9, 9);
            this.pictureBoxBinaryTree.Name = "pictureBoxBinaryTree";
            this.pictureBoxBinaryTree.Size = new System.Drawing.Size(2066, 731);
            this.pictureBoxBinaryTree.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxBinaryTree.TabIndex = 0;
            this.pictureBoxBinaryTree.TabStop = false;
            // 
            // tabPageTruthTable
            // 
            this.tabPageTruthTable.Controls.Add(this.listboxTruthTable);
            this.tabPageTruthTable.Location = new System.Drawing.Point(4, 41);
            this.tabPageTruthTable.Name = "tabPageTruthTable";
            this.tabPageTruthTable.Size = new System.Drawing.Size(2110, 746);
            this.tabPageTruthTable.TabIndex = 2;
            this.tabPageTruthTable.Text = "Truth table";
            this.tabPageTruthTable.UseVisualStyleBackColor = true;
            // 
            // listboxTruthTable
            // 
            this.listboxTruthTable.FormattingEnabled = true;
            this.listboxTruthTable.ItemHeight = 32;
            this.listboxTruthTable.Location = new System.Drawing.Point(3, 2);
            this.listboxTruthTable.Name = "listboxTruthTable";
            this.listboxTruthTable.Size = new System.Drawing.Size(2075, 740);
            this.listboxTruthTable.TabIndex = 0;
            // 
            // tabPageSimplifiedTruthTable
            // 
            this.tabPageSimplifiedTruthTable.Controls.Add(this.listboxSimplifiedTruthTable);
            this.tabPageSimplifiedTruthTable.Location = new System.Drawing.Point(4, 41);
            this.tabPageSimplifiedTruthTable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageSimplifiedTruthTable.Name = "tabPageSimplifiedTruthTable";
            this.tabPageSimplifiedTruthTable.Size = new System.Drawing.Size(2110, 746);
            this.tabPageSimplifiedTruthTable.TabIndex = 3;
            this.tabPageSimplifiedTruthTable.Text = "Simplified truth table";
            this.tabPageSimplifiedTruthTable.UseVisualStyleBackColor = true;
            // 
            // listboxSimplifiedTruthTable
            // 
            this.listboxSimplifiedTruthTable.FormattingEnabled = true;
            this.listboxSimplifiedTruthTable.ItemHeight = 32;
            this.listboxSimplifiedTruthTable.Location = new System.Drawing.Point(3, 5);
            this.listboxSimplifiedTruthTable.Name = "listboxSimplifiedTruthTable";
            this.listboxSimplifiedTruthTable.Size = new System.Drawing.Size(2075, 740);
            this.listboxSimplifiedTruthTable.TabIndex = 1;
            // 
            // tabPageNormalizedTruthTable
            // 
            this.tabPageNormalizedTruthTable.Controls.Add(this.listboxNormalizedTruthTable);
            this.tabPageNormalizedTruthTable.Location = new System.Drawing.Point(4, 41);
            this.tabPageNormalizedTruthTable.Name = "tabPageNormalizedTruthTable";
            this.tabPageNormalizedTruthTable.Size = new System.Drawing.Size(2110, 746);
            this.tabPageNormalizedTruthTable.TabIndex = 4;
            this.tabPageNormalizedTruthTable.Text = "Normalized truth table";
            this.tabPageNormalizedTruthTable.UseVisualStyleBackColor = true;
            // 
            // listboxNormalizedTruthTable
            // 
            this.listboxNormalizedTruthTable.FormattingEnabled = true;
            this.listboxNormalizedTruthTable.ItemHeight = 32;
            this.listboxNormalizedTruthTable.Location = new System.Drawing.Point(3, 3);
            this.listboxNormalizedTruthTable.Name = "listboxNormalizedTruthTable";
            this.listboxNormalizedTruthTable.Size = new System.Drawing.Size(2075, 740);
            this.listboxNormalizedTruthTable.TabIndex = 2;
            // 
            // tabPageNormalizedSimplifiedTable
            // 
            this.tabPageNormalizedSimplifiedTable.Controls.Add(this.listboxNormalizedSimplifiedTruthTable);
            this.tabPageNormalizedSimplifiedTable.Location = new System.Drawing.Point(4, 41);
            this.tabPageNormalizedSimplifiedTable.Name = "tabPageNormalizedSimplifiedTable";
            this.tabPageNormalizedSimplifiedTable.Size = new System.Drawing.Size(2110, 746);
            this.tabPageNormalizedSimplifiedTable.TabIndex = 5;
            this.tabPageNormalizedSimplifiedTable.Text = "Normalized simplified truth table";
            this.tabPageNormalizedSimplifiedTable.UseVisualStyleBackColor = true;
            // 
            // listboxNormalizedSimplifiedTruthTable
            // 
            this.listboxNormalizedSimplifiedTruthTable.FormattingEnabled = true;
            this.listboxNormalizedSimplifiedTruthTable.ItemHeight = 32;
            this.listboxNormalizedSimplifiedTruthTable.Location = new System.Drawing.Point(3, 3);
            this.listboxNormalizedSimplifiedTruthTable.Name = "listboxNormalizedSimplifiedTruthTable";
            this.listboxNormalizedSimplifiedTruthTable.Size = new System.Drawing.Size(2075, 740);
            this.listboxNormalizedSimplifiedTruthTable.TabIndex = 2;
            // 
            // buttonGetRandomFormulaFromCollection
            // 
            this.buttonGetRandomFormulaFromCollection.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGetRandomFormulaFromCollection.Location = new System.Drawing.Point(12, 265);
            this.buttonGetRandomFormulaFromCollection.Name = "buttonGetRandomFormulaFromCollection";
            this.buttonGetRandomFormulaFromCollection.Size = new System.Drawing.Size(174, 145);
            this.buttonGetRandomFormulaFromCollection.TabIndex = 6;
            this.buttonGetRandomFormulaFromCollection.Text = "Calculate random formula";
            this.buttonGetRandomFormulaFromCollection.UseVisualStyleBackColor = true;
            this.buttonGetRandomFormulaFromCollection.Click += new System.EventHandler(this.buttonGetRandomFormulaFromCollection_Click);
            // 
            // buttonEditList
            // 
            this.buttonEditList.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditList.Location = new System.Drawing.Point(12, 416);
            this.buttonEditList.Name = "buttonEditList";
            this.buttonEditList.Size = new System.Drawing.Size(174, 145);
            this.buttonEditList.TabIndex = 7;
            this.buttonEditList.Text = "Edit list";
            this.buttonEditList.UseVisualStyleBackColor = true;
            this.buttonEditList.Click += new System.EventHandler(this.buttonEditList_Click);
            // 
            // buttonCalculateNextOwnFormula
            // 
            this.buttonCalculateNextOwnFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculateNextOwnFormula.Location = new System.Drawing.Point(12, 567);
            this.buttonCalculateNextOwnFormula.Name = "buttonCalculateNextOwnFormula";
            this.buttonCalculateNextOwnFormula.Size = new System.Drawing.Size(174, 334);
            this.buttonCalculateNextOwnFormula.TabIndex = 8;
            this.buttonCalculateNextOwnFormula.Text = "Calculate next formula from list";
            this.buttonCalculateNextOwnFormula.UseVisualStyleBackColor = true;
            this.buttonCalculateNextOwnFormula.Click += new System.EventHandler(this.buttonCalculateNextOwnFormula_Click);
            // 
            // LogicalPropositionsHandling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2329, 920);
            this.Controls.Add(this.buttonCalculateNextOwnFormula);
            this.Controls.Add(this.buttonEditList);
            this.Controls.Add(this.buttonGetRandomFormulaFromCollection);
            this.Controls.Add(this.tabControlCalculationResults);
            this.Controls.Add(this.btnCalculateStuff);
            this.Controls.Add(this.rtbInputFormula);
            this.Controls.Add(this.lblInputFormula);
            this.Name = "LogicalPropositionsHandling";
            this.Text = "Logical Propositions Handling";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LogicalPropositionsHandling_FormClosing);
            this.tabControlCalculationResults.ResumeLayout(false);
            this.tabPageResultsData.ResumeLayout(false);
            this.tabPageResultsData.PerformLayout();
            this.tabPageBinaryTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBinaryTree)).EndInit();
            this.tabPageTruthTable.ResumeLayout(false);
            this.tabPageSimplifiedTruthTable.ResumeLayout(false);
            this.tabPageNormalizedTruthTable.ResumeLayout(false);
            this.tabPageNormalizedSimplifiedTable.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblInputFormula;
        private System.Windows.Forms.RichTextBox rtbInputFormula;
        private System.Windows.Forms.Button btnCalculateStuff;
        private System.Windows.Forms.TabControl tabControlCalculationResults;
        private System.Windows.Forms.TabPage tabPageBinaryTree;
        private System.Windows.Forms.PictureBox pictureBoxBinaryTree;
        private System.Windows.Forms.TabPage tabPageResultsData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxVariablesInFormula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxResultInfixNotation;
        private System.Windows.Forms.Button buttonGetRandomFormulaFromCollection;
        private System.Windows.Forms.TabPage tabPageTruthTable;
        private System.Windows.Forms.TextBox textBoxHashCodeResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPageSimplifiedTruthTable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listboxTruthTable;
        private System.Windows.Forms.ListBox listboxSimplifiedTruthTable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPageNormalizedTruthTable;
        private System.Windows.Forms.ListBox listboxNormalizedTruthTable;
        private System.Windows.Forms.TabPage tabPageNormalizedSimplifiedTable;
        private System.Windows.Forms.ListBox listboxNormalizedSimplifiedTruthTable;
        private System.Windows.Forms.RichTextBox textboxNormalizedFormula;
        private System.Windows.Forms.RichTextBox textboxNormalizedInfix;
        private System.Windows.Forms.RichTextBox textboxNandification;
        private System.Windows.Forms.Button buttonEditList;
        private System.Windows.Forms.Button buttonCalculateNextOwnFormula;
    }
}

